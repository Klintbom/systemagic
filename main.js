var module = require('pavlovjs');

var R = require("ramda");
const readline = require('readline');


var pavlov = new module.Pavlov();

// transitions and rewards
var state = " " + " " + " " +
            " " + " " + " " +
            " " + " " + " " ;
var printState = function(state){
  state = state.split("");
  console.log("*---*");
  console.log("|" + state[0] + state[1] + state[2] + "|");
  console.log("|" + state[3] + state[4] + state[5] + "|");
  console.log("|" + state[6] + state[7] + state[8] + "|");
  console.log("*---*");

};


var generateMove = function(state){
  while (true) {
    var rand = Math.round(Math.random()*8);
    if (state[rand] === " "){
      return rand;
    }
  }
};



var applyMove = function(state, move, symbol){
  state = state.split("");
  state[move] = symbol;
  return state.join("");
};

var addMove = function(state){
  state = state.split("");
  var newState = R.clone(state);
  var playerAction = generateMove(state);

  newState[playerAction] = "x";

  if (newState.indexOf(" ") !== -1){
    newState[generateMove(newState)] = "o";
  }

  //console.log("|" + state.join("") + "|", playerAction, "|" + newState.join("") + "|");
  pavlov.transition({ "state":  state.join(""), "action": playerAction + "", "state_": newState.join("") });
  return newState.join("");
};

var whoWon = function(state){
  if (state.indexOf(" ") === -1) {
    return null;
  }

  state = state.split("");

  if (state[0] === "x" && state[1] === "x" && state[2] === "x"){
    return "x";
  }
  if (state[3] === "x" && state[4] === "x" && state[5] === "x"){
    return "x";
  }
  if (state[6] === "x" && state[7] === "x" && state[8] === "x"){
    return "x";
  }
  if (state[0] === "x" && state[3] === "x" && state[6] === "x"){
    return "x";
  }
  if (state[1] === "x" && state[4] === "x" && state[7] === "x"){
    return "x";
  }
  if (state[2] === "x" && state[5] === "x" && state[8] === "x"){
    return "x";
  }
  if (state[0] === "x" && state[4] === "x" && state[8] === "x"){
    return "x";
  }
  if (state[6] === "x" && state[4] === "x" && state[2] === "x"){
    return "x";
  }

  if (state[0] === "o" && state[1] === "o" && state[2] === "o"){
    return "o";
  }
  if (state[3] === "o" && state[4] === "o" && state[5] === "o"){
    return "o";
  }
  if (state[6] === "o" && state[7] === "o" && state[8] === "o"){
    return "o";
  }
  if (state[0] === "o" && state[3] === "o" && state[6] === "o"){
    return "o";
  }
  if (state[1] === "o" && state[4] === "o" && state[7] === "o"){
    return "o";
  }
  if (state[2] === "o" && state[5] === "o" && state[8] === "o"){
    return "o";
  }
  if (state[0] === "o" && state[4] === "o" && state[8] === "o"){
    return "o";
  }
  if (state[6] === "o" && state[4] === "o" && state[2] === "o"){
    return "o";
  }
};

var startState = "         ";

var xWins = 0;
var oWins = 0;

for (var i = 0; i < 100; i++) {
  var newState = startState;
  while (true){
    //printState(newState);
    newState = addMove(newState);

    var winner = whoWon(newState);
    if (winner === "x") {
      //console.log("x won");
      pavlov.reward(1);
      xWins++;
      break;
    }
    if (winner === "o") {
      //console.log("o won");
      pavlov.reward(0);

      oWins++;
      break;
    }
    if (winner === null) {
      pavlov.reward(0);

      //console.log("no winner");
      break;
    }
  }
}

console.log("DONE, x: " + xWins + " o:" + oWins);

// learn from observations
pavlov.learn();

//policy


var state =  " " + " " + " " +
             " " + " " + " " +
             " " + " " + " " ;

var move1 = pavlov.action(state);
console.log("CPMove:", move1);
state = applyMove(state, move1, "x")



const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


printState(state);
rl.question("", (answer) => {
  state = applyMove(state, answer, "o");

  var move2 = pavlov.action(state);
  state = applyMove(state, move2, "x")

  console.log("CPMove:", move2);
  printState(state);
  rl.question("", (answer) => {

    state = applyMove(state, answer, "o");

    var move3 = pavlov.action(state);
    state = applyMove(state, move3, "x")

    console.log("CPMove:", move3);

    printState(state);
      rl.question("", (answer) => {

        state = applyMove(state, answer, "o");

        var move4 = pavlov.action(state);
        state = applyMove(state, move4, "x")

        console.log("CPMove:", move4);
        printState(state);

        rl.close();
      });

    //rl.close();
  });

  //rl.close();
});
